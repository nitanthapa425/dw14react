import React from "react";
import Info from "./roughComponent/Info";
import College from "./roughComponent/College";
import TernaryOperator from "./roughComponent/TernaryOperator";
import BehaviourOfData from "./roughComponent/BehaviourOfData";
import ButtonClick from "./roughComponent/ButtonClick";
import LearnUseState1 from "./roughComponent/learnUseState/LearnUseState1";
import LearnUseState2 from "./roughComponent/learnUseState/LearnUseState2";
//Component

/* 
Component
    Function
    first Letter is capital
    call just like tag

*/

const App1 = () => {
  return (
    <div>
      <Info name="nitan" address="gagalphedi" age={30} isMarried={true}></Info>
      {/* <College></College> */}
      {/* <TernaryOperator></TernaryOperator> */}
      {/* <BehaviourOfData
        name="nitan"
        age={30}
        isMarried={false}
        favFood={["chicken", "mutton"]}
        fatherInfo={{ name: "shiva", age: 65 }}
        myTag={<div>I am div</div>}
      ></BehaviourOfData> */}
      {/* <ButtonClick></ButtonClick> */}

      {/* <LearnUseState1></LearnUseState1> */}
      {/* <LearnUseState2></LearnUseState2> */}
    </div>
  );
};

export default App1;
