import React, { useState } from "react";

const LearnUseState1 = () => {
  //   defining variable using useState
  let [name, setName] = useState("nitan"); //"nitan"//"ram"

  const handleClick = (e) => {
    setName("ram");
  };

  return (
    <div>
      name is {name}
      <br></br>
      <button onClick={handleClick}>Change Name</button>
    </div>
  );
};

export default LearnUseState1;
