import React from "react";

// if age is greater tha 18 ("he can enter bar") else he can not enter bar

// (age === 16)  your age is 16
//          (age === 17)  your age is 17
//          (age === 18)  your age is 18
//         your age is neither 16 ,17,18

// if isMarried===true "he is married" else "he is unmarried"

// if isMarried ===true  "he is married"

//if gender ==="male" "he is male",
//if gender==="female" "she is female"
//if gender==="other"  "they are other"

const TernaryOperator = () => {
  let age = 17;

  let isMarried = false;
  let output = isMarried === true ? "he is married" : null;
  console.log(output);

  // let output =
  //   age === 16
  //     ? "your age is 16"
  //     : age === 17
  //     ? "your age is 17"
  //     : age === 18
  //     ? "your age is 18"
  //     : "your age is neither 16, 17 18";

  // console.log(output);

  //   if (age >= 18) {
  //     console.log("he can enter bar");
  //   } else {
  //     console.log("he can not enter bar");
  //   }

  //   let output = age >= 18 ? "he can enter bar" : "he can not enter bar";
  //   console.log(output);

  return <div>TernaryOperator</div>;
};

export default TernaryOperator;
