import React from "react";

const BehaviourOfData = ({
  name,
  age,
  isMarried,
  favFood,
  fatherInfo,
  myTag,
}) => {
  //fatherInfo = {name:"shiva",age:65}
  return (
    <div>
      {name}
      <br></br>
      {age}
      <br></br>
      {isMarried ? "yes" : "no"}
      <br></br>
      {favFood}
      <br></br>
      {fatherInfo.name}
      <br></br>
      {fatherInfo.age}
      <br></br>
      {myTag}
    </div>
  );
};

export default BehaviourOfData;
