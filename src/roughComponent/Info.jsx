const Info = ({ name, address, age, isMarried }) => {
  return (
    <div>
      <p>my name is {name}</p>
      <p>i live in {address}</p>
      <p>i am {age}</p>
      <p>isMarried is {isMarried ? "yes" : "no"}</p>
    </div>
  );
};

export default Info;
